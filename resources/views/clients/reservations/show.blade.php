@php $link = 'reservations' @endphp
@extends('clients.layout')

@section('title') Réservation N° {{ $reservation->id }} @endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Réservation N° {{ $reservation->id }}
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Date:</label>
                        <input type="text" readonly class="form-control" value="{{ date('Y-m-d' , strtotime($reservation->date)) }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Memebres:</label>
                        <input type="text" readonly class="form-control" value="{{ date('Y-m-d' , strtotime($reservation->members)) }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Réservations</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('clients.dashboard') }}">Réservations</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('clients.reservations.index') }}">{{ $reservation->id }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js') }}"></script>
@endsection

@section('js')
    <script>
        @if(session()->has('add')) toastr.info('Vous avez passer une réservation. Notre équipe va vous comuniquer.') @endif
        @if(session()->has('activate')) toastr.success('You have been activated the item {{ session()->get('activate') }}.') @endif
        @if(session()->has('deactivate')) toastr.warning('You have been deactivate the item {{ session()->get('deactivate') }}.')  @endif
    </script>
@endsection
