@php $link = 'reservations' @endphp
@extends('clients.layout')

@section('title') Réservations @endsection

@section('content')
    <div class="card mt-2">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Members</th>
                    <th>Created at</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reservations as $reservation)
                    <tr>
                        <td>{{ $reservation->id }}</td>
                        <td class="text-capitalize">{{ date('Y-m-d' , strtotime($reservation->date)) }}</td>
                        <td>{{ $reservation->members }}</td>
                        <td>{{ date('Y-m-d' , strtotime($reservation->created_at)) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Réservations</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('clients.dashboard') }}">Réservations</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    @endsection

@section('scripts')

    @endsection

@section('js')

    @endsection
