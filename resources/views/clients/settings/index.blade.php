@php $link = 'settings' @endphp
@extends('clients.layout')

@section('title') Paramètres @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <button form="update" class="btn btn-sm btn-primary"><i class="fa fa-save"></i></button>
    </div>
    <div class="card mt-2">
        <div class="card-header">
            {{ $user->full_name }}
        </div>
        <div class="card-body">
            <form action="{{ route('clients.settings.update' , ['setting' => -1]) }}" method="post" id="update">
                @csrf   @method('PUT')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nom</label>
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" value="{{ $user->last_name }}" name="last_name">
                            @error('last_name') <small class="text-center text-danger btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror" value="{{ $user->first_name }}" name="first_name">
                            @error('first_name') <small class="text-center text-danger btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Prénom</label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ $user->email }}" name="email">
                            @error('email') <small class="text-center text-danger btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mot de passe</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                            @error('password') <small class="text-center text-danger btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Confirmation du mot de passe</label>
                            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">
                            @error('password_confirmation') <small class="text-center text-danger btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Paramètres</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('clients.settings.index') }}">Paramères</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection
