<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar nav-legacy flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('clients.dashboard') }}" class="nav-link {{ $link == 'dashboard' ? 'active' : '' }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Tableau de bord
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('clients.reservations.index') }}" class="nav-link {{ $link == 'reservations' ? 'active' : '' }}">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Réservations
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('clients.settings.index') }}" class="nav-link {{ $link == 'settings' ? 'active' : '' }}">
                <i class="nav-icon fas fa-cogs"></i>
                <p>
                    Paramètres
                </p>
            </a>
        </li>
</nav>
<!-- /.sidebar-menu -->
