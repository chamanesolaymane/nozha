<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} | Registration Page</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">

<div class="register-box">
    <div class="register-logo">
        <a href="{{ url('/') }}"><b>{{ config('app.name') }}</b></a>
    </div>

    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Céer un compte</p>

            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="input-group mb-1">
                    <input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control @error('last_name') is-invalid @enderror" placeholder="Nom">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                @error('last_name') <small class="text-danger">{{ $message }}</small> @enderror
                <div class="input-group mb-1 mt-2">
                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control @error('first_name') is-invalid @enderror" placeholder="Prénom">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                @error('first_name') <small class="text-danger">{{ $message }}</small> @enderror
                <div class="input-group mb-1 mt-2">
                    <input type="text" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                @error('email') <small class="text-danger">{{ $message }}</small> @enderror
                <div class="input-group mb-1 mt-2">
                    <select name="code" id="codes" onchange="fillPhone()" class="text-sm form-control" style="border-bottom-right-radius: 0px; border-right: none">
                        @foreach(array_keys(countries()) as $country)
                            <option {{ old('code') == $country ? 'selected' : '' }} value="{{ country($country)->getCallingCode() }}">{{ country($country)->getName() }} ({{ country($country)->getCallingCode() }})</option>
                        @endforeach
                    </select>
                    <input type="text" id="number" onchange="fillPhone()" value="{{ old('phone') }}" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone" style="border-bottom-left-radius: 0px; border-left: none">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-phone"></span>
                        </div>
                    </div>
                    <input type="hidden" name="phone" id="phone">
                </div>
                @error('phone') <small class="text-danger">{{ $message }}</small> @enderror
                <div class="input-group mb-1 mt-2">
                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Mot de passe">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                @error('password') <small class="text-danger">{{ $message }}</small> @enderror
                <div class="input-group mb-1 mt-2">
                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Retype password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                @error('password_confirmation') <small class="text-danger">{{ $message }}</small> @enderror
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="agreeTerms" name="terms"  value="agree">
                            <label for="agreeTerms">
                                I agree to the <a href="#">terms</a>
                            </label>
                        </div>
                        @error('terms') <small class="text-danger">{{ $message }}</small> @enderror
                    </div>
                    <!-- /.col -->
                    <div class="col-4 mt-2">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enrgistrer</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>


            <a href="{{ route('login.form') }}" class="text-center">Se connecter</a>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<script>
    function fillPhone(){
        $('#phone').val('+'+$('#codes').val() + $('#number').val())
    }
</script>
</body>
</html>
