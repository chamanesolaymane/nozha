<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} | @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @yield('style')
    <style>
        body{
            overflow-x: hidden;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed sidebar-collapse">
<div class="wrapper text-sm">

    <!-- Navbar -->
    <nav class=" navbar navbar-expand text-sm navbar-dark">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="{{ url('/') }}" class="brand-link">
                    <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                         style="opacity: .8">
                    <span class="brand-text font-weight-light"></span>
                </a>
            </li>
        </ul>

        <form class="form-inline ml-3 w-50" method="post" action="{{ route('landing.query') }}"> @csrf
            <div class="input-group col-md-8 col-lg-8 col-12 offset-sm-0">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" name="query">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto pr-5">
            <!-- Notifications Dropdown Menu -->
           @auth
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('clients.dashboard') }}" class="nav-link"><i class="fa fa-home"></i> Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('logout') }}" class="nav-link"><i class="fa fa-power-off"></i> Déconnecter</a>
                </li>
               @else
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('login.form') }}" class="nav-link"><i class="fa fa-user-lock"></i> Se Connecter</a>
                </li>
                <li class="nav-item d-md-none d-lg-none">
                    <a href="{{ route('login.form') }}" class="nav-link"><i class="fa fa-user-lock"></i></a>
                </li>

                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('register.form') }}" class="nav-link"><i class="fa fa-user-plus"></i> Créer un compte</a>
                </li>
                <li class="nav-item d-md-none d-lg-none">
                    <a href="{{ route('register.form') }}" class="nav-link"><i class="fa fa-user-plus"></i></a>
                </li>
            @endauth
        </ul>
    </nav>
    <!-- /.navbar -->

    <div class="content">
        <!-- Content Header (Page header) -->
    <!-- /.content-header -->

        <!-- Main content -->
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-light">
                        <div class="card-header p-0 pt-1 row">
                            <ul class="nav nav-tabs col-md-10 offset-md-1 col-sm-12" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item col-md-2 text-center">
                                    <a class="nav-link {{ $link == 'index' ? 'active' : '' }}" href="{{ url('/') }}" aria-controls="custom-tabs-one-home" aria-selected="true">Notre Séelection <i class="fa fa-star"></i></a>
                                </li>
                                <li class="nav-item col-md-2 text-center">
                                    <a class="nav-link {{ $link == 'activities' ? 'active' : '' }}"  href="{{ route('landing.type' , ['type' => 'activities']) }}" aria-controls="custom-tabs-one-profile" aria-selected="false">Activités <i class="fa fa-map-marker"></i></a>
                                </li>
                                <li class="nav-item col-md-2 text-center">
                                    <a class="nav-link {{ $link == 'restaurants' ? 'active' : '' }}"  href="{{ route('landing.type' , ['type' => 'restaurants']) }} " aria-controls="custom-tabs-one-messages" aria-selected="false">Resaturants <i class="fa fa-utensils"></i></a>
                                </li>
                                <li class="nav-item col-md-2 text-center">
                                    <a class="nav-link {{ $link == 'hotels' ? 'active' : '' }}"  href="{{ route('landing.type' , ['type' => 'hotels']) }} " aria-controls="custom-tabs-one-settings" aria-selected="false">Hôtels <i class="fa fa-hotel"></i></a>
                                </li>
                                <li class="nav-item col-md-2 text-center">
                                    <a class="nav-link {{ $link == 'coffees' ? 'active' : '' }}"  href="{{ route('landing.type' , ['type' => 'coffees']) }} " aria-controls="custom-tabs-one-settings" aria-selected="false">Cafés <i class="fa fa-coffee"></i></a>
                                </li>
                                <li class="nav-item col-md-2 text-center">
                                    <a class="nav-link {{ $link == 'health' ? 'active' : '' }}"  href="{{ route('landing.type' , ['type' => 'health']) }} " aria-controls="custom-tabs-one-settings" aria-selected="false">Santé & SPA <i class="fa fa-heart"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="mt-2">
                <div class="row">
                    @yield('search')

                    @yield('content')
                </div>
            </div><!-- /.container-fluid -->
        <!-- /.content -->
    </div>
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Summernote -->
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    @yield('js')
</div>
</body>
</html>
