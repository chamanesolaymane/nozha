@php $link = $type ?? 'index'; @endphp
@extends('welcome')

@section('title')
    @if($type)
        @if($type == 'activities')
            Activités
            @elseif($type == 'restaurants')
            Restaurants
        @elseif($type == 'hotels')
            Hôtels
        @elseif($type == 'coffees')
            Cafés
        @else
            Santé & SPA
            @endif
    @else
        Notre Sélection
    @endif
@endsection

@section('content')
    <div class="col-md-9">
        <!-- Default box -->
        <div class="card card-solid">
            <div class="card-body pb-0">
                <div class="row d-flex align-items-stretch">
                    @foreach($offers as $offer)
                        <div class="col-12 d-flex align-items-stretch">
                            <div class="card bg-light">
                                <div class="card-header text-muted border-bottom-0">

                                </div>
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 text-center">
                                            <a href="{{ route('landing.show' , ['id' => $offer->id]) }}">
                                                <img src="{{ asset('items/avatars/'.$offer->item->avatar) }}" alt="" class="img-fluid img-fluid">
                                            </a>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <a href="{{ route('landing.show' , ['id' => $offer->id]) }}">
                                                <h2 class="lead d-inline-block"><b>{{ $offer->item->name  }}</b>
                                                    @if($offer->impressions()->avg('impression') == 0)
                                                        <i class="fa fa-star text-warning"></i>
                                                    @else
                                                        @for($i = 0; $i < ($offer->impressions()->avg('impression')); $i++)
                                                            <i class="fa fa-star text-warning"></i>
                                                        @endfor
                                                    @endif</h2>
                                                <p class="text-sm">{{ $offer->title }}</p>
                                            </a>
                                            <p>{{ $offer->item->address }}</p>
                                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                                <li><b>Prix:</b> {{ $offer->price }} MAD</li>
                                                <li><b>Remise:</b> {{ $offer->discount }} %</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        @auth <a href="" onclick="$('.offer').val({{ $offer->id }})" class="btn btn-default" data-toggle="modal" data-target="#reservation">@else <a href="{{ route('login.form') }}" class="btn btn-default"> @endauth
                                            <i class="fas fa-shopping-cart"></i> Réserver
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <nav aria-label="Contacts Page Navigation">
                    <ul class="pagination justify-content-center m-0">
                        {{ $offers->links() }}
                    </ul>
                </nav>
            </div>
            <!-- /.card-footer -->
        </div>
        <!-- /.card -->
    </div>
    <div class="modal fade" id="reservation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nouvelle Réservation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('clients.reservations.store') }}" id="store" method="post">
                        @csrf <input type="hidden" class="offer" name="offer" value="{{ old('offer') }}">
                        <div class="form-group">
                            <label>Date:</label>
                            <input type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ old('date') ?? \Carbon\Carbon::now()->addDays()->format('Y-m-d') }}">
                            @error('date') <small class="text-danger text-center btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                        <div class="form-group">
                            <label>Nomber des memebres:</label>
                            <input type="number" class="form-control @error('members') is-invalid @enderror"  name="members" value="{{ old('members') ?? 1 }}">
                            @error('members') <small class="text-danger text-center btn-block mt-2">{{ $message }}</small> @enderror
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="$('#store').submit()" class="btn btn-primary"><i class="fa fa-check"></i> Réserver</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @endsection

@section('search')
   <div class="col-md-3">
       <div class="row d-flex ml-3 ml-2">
           <div class="card col-12">
               <div class="card-body text-center">
                   <form action="{{ route('landing.search') }}" method="post">
                       @csrf
                       <div class="row">
                           <div class="col-12">
                               <div class="form-group">
                                   <label>Ville</label>
                                   <select name="city" class="form-control form-control-sm" id="">
                                       <option {{ old('city') == 'casa' ? 'selected' : '' }} value="casa"> Casablanca</option>
                                       <option {{ old('city') == 'marrakech' ? 'selected' : '' }} value="marrakech"> Marrakech</option>
                                       <option {{ old('city') == 'rabat' ? 'selected' : '' }} value="rabat"> Rabat</option>
                                       <option {{ old('city') == 'tangier' ? 'selected' : '' }} value="tangier"> Tanger</option>
                                       <option {{ old('city') == 'agadir' ? 'selected' : '' }} value="agadir"> Agadir</option>
                                       <option {{ old('city') == 'fes' ? 'selected' : '' }} value="fes"> Fes</option>
                                       <option {{ old('city') == 'ouarzazate' ? 'selected' : '' }} value="ouarzazate"> Ouarzazate</option>
                                       <option {{ old('city') == 'essaouira' ? 'selected' : '' }} value="essaouira"> Essaouira</option>
                                   </select>
                               </div>
                           </div>
                           <div class="col-2">
                               <button class="btn btn-primary mt-4"><i class="fa fa-search"></i></button>
                           </div>
                       </div>
                   </form>
               </div>
           </div>
       </div>
   </div>
    @endsection

@section('js')
    <script>
        @if($errors->any())
            $('#reservation').modal('show')
            @endif
    </script>
    @endsection

