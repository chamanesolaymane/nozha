@php  $link = ''; @endphp
@extends('welcome')

@section('title') {{ $offer->title }} @endsection

@section('content')
  <div class="col-md-12 mr-4">
      <div class="row d-flex justify-content-center">
          <div class="col-md-8">
              <div class="card card-solid">
                  <div class="card-body">
                      <div class="row">
                          <div class="col-12 col-sm-12">
                              <h3 class="d-inline-block">{{ $offer->item->name }}: {{ $offer->title }}

                                  @if($offer->impressions()->avg('impression') == 0)
                                      <i class="fa fa-star text-warning"></i>
                                  @else
                                      @for($i = 0; $i < ($offer->impressions()->avg('impression')); $i++)
                                          <i class="fa fa-star text-warning"></i>
                                      @endfor
                                  @endif


                              </h3>
                              <div class="col-12">
                                  <img src="{{ asset('offers/covers/'.$offer->cover) }}" style="width: 90%; height: 400px" class="product-image" alt="Product Image">
                              </div>
                          </div>
                          <div class="col-12 col-sm-12">
                              <h4 class="my-3">Offer</h4>
                              {!! $offer->offer !!}
                          </div>
                          <div class="col-12 col-sm-12">
                              <h4 class="my-3">Description</h4>
                              {!! $offer->description !!}
                          </div>
                          <div class="col-12 col-sm-12">
                              <h4 class="my-3">Commentaires</h4>
                             @if($offer->comments()->count() != 0)
                                  @foreach($offer->comments()->orderByDesc('id')->get() as $comment)
                                      <div class="post">
                                          <div class="user-block">
                                      <span class="username">
                                        <a href="#">{{ $comment->user->full_name }}.</a>
                                      </span>
                                              <span class="description">{{ date('d/m/Y' , strtotime($comment->created_at)) }}</span>
                                          </div>
                                          <!-- /.user-block -->
                                          <p>
                                              {{ $comment->text }}
                                          </p>
                                      </div>
                                  @endforeach
                                 @else
                              <h4 class="text-center text-muted">Aucune Commentaire</h4>
                              @endif
                          </div>
                          <div class="col-md-12">
                              <form action="{{ route('comments.store') }}" method="post">
                                  @csrf
                                  <div class="post-form">
                                      <input type="hidden" name="offer" value="{{ $offer->id }}">
                                      <input class="form-control form-control-sm" type="text" name="text" placeholder="Taper votre commantaire">
                                      @error('text') <small class="text-danger text-center">{{ $message }}</small> @enderror
                                      @auth <button type="submit" class="btn btn-primary text-white float-right mt-2"><i class="fa fa-comment"></i></button>
                                      @else
                                          <a href="{{ route('login.form') }}" class="btn btn-primary text-white float-right mt-2"><i class="fa fa-comment"></i></a>
                                      @endauth
                                  </div>
                              </form>
                          </div>
                          @if($offer->video)
                              <div class="col-12 d-flex justify-content-center">
                                  <h5>{{ $offer->video_title }}</h5>
                                  <video src="{{ $offer->video }}"></video>
                              </div>
                              @endif
                      </div>
                  </div>
                  <!-- /.card-body -->
              </div>
          </div>
          <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-inline-flex">
                            <h3>{{ $offer->price }} MAD</h3> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <p class="mt-2"> {{ $offer->discount }}% de remise</p>
                        </div>
                        <div class="dropdown-divider"></div>
                        @auth <a href="" onclick="$('.offer').val({{ $offer->id }})" class="btn btn-block btn-lg btn-primary" data-toggle="modal" data-target="#reservation">@else <a href="{{ route('login.form') }}" class="btn btn-block btn-lg btn-primary"> @endauth
                                <i class="fas fa-shopping-cart"></i> Réserver
                            </a>
                    </div>
                </div>
              <div class="card">
                    <div class="card-body">
                        <h5>Adresse</h5>
                        <p>{{ $offer->item->address }}</p>
                    </div>
              </div>
              <div class="card">
                  <div class="card-body">
                      <h5>Geolocalisaiton</h5>
                      <div class="row">
                          <div class="col-md-12">
                              <div id="map" class="d-block" style="width: 100%; height: 200px"></div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="card">
                  <div class="card-body">
                      <h5>Impression</h5>
                      <p class="d-inline-block">
                          <i class="far fa-star fa-2x text-warning star" id="star1" style="cursor: pointer" @if(\Illuminate\Support\Facades\Auth::check()) onclick="fillStars(1)" @else onclick="window.location  = '{{ route('login.form') }}'" @endif></i>
                          <i class="far fa-star fa-2x text-warning star" id="star2" style="cursor: pointer" @if(\Illuminate\Support\Facades\Auth::check()) onclick="fillStars(2)" @else onclick="window.location  = '{{ route('login.form') }}'" @endif></i>
                          <i class="far fa-star fa-2x text-warning star" id="star3" style="cursor: pointer" @if(\Illuminate\Support\Facades\Auth::check()) onclick="fillStars(3)" @else onclick="window.location  = '{{ route('login.form') }}'" @endif></i>
                          <i class="far fa-star fa-2x text-warning star" id="star4" style="cursor: pointer" @if(\Illuminate\Support\Facades\Auth::check()) onclick="fillStars(4)" @else onclick="window.location  = '{{ route('login.form') }}'" @endif></i>
                          <i class="far fa-star fa-2x text-warning star" id="star5" style="cursor: pointer" @if(\Illuminate\Support\Facades\Auth::check()) onclick="fillStars(5)" @else onclick="window.location  = '{{ route('login.form') }}'" @endif></i>
                          <i class="far fa-star fa-2x text-warning star" id="star6" style="cursor: pointer" @if(\Illuminate\Support\Facades\Auth::check()) onclick="fillStars(6)" @else onclick="window.location  = '{{ route('login.form') }}'" @endif></i>
                      </p>
                      <form action="">
                          <input type="hidden" name="impression" id="impression">
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="reservation">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Nouvelle Réservation</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <form action="{{ route('clients.reservations.store') }}" id="store" method="post">
                      @csrf <input type="hidden" name="offer" value="{{ $offer->id }}">
                      <div class="form-group">
                          <label>Date:</label>
                          <input type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ old('date') ?? \Carbon\Carbon::now()->addDays()->format('Y-m-d') }}">
                          @error('date') <small class="text-danger text-center btn-block mt-2">{{ $message }}</small> @enderror
                      </div>
                      <div class="form-group">
                          <label>Nomber des memebres:</label>
                          <input type="number" class="form-control @error('members') is-invalid @enderror"  name="members" value="{{ old('members') ?? 1 }}">
                          @error('members') <small class="text-danger text-center btn-block mt-2">{{ $message }}</small> @enderror
                      </div>
                  </form>
              </div>
              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" onclick="$('#store').submit()" class="btn btn-primary"><i class="fa fa-check"></i> Réserver</button>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
@endsection

@section('js')
    <script>
        var first = false
        fillStars({{ $offer->impressions()->where('user_id' , \Illuminate\Support\Facades\Auth::id())->count() !=0 && \Illuminate\Support\Facades\Auth::check() ? $offer->impressions()->where('user_id' , \Illuminate\Support\Facades\Auth::id())->first()->impression : 0 }})
        function fillStars(index) {
            $('.star').removeClass('fa')
            for(var i = 0 ; i < index; i++)
                $('#star'+(i+1)).addClass('fa')
          if(first)
              $.ajax({
                  url : '{{ route('impressions.store') }}',
                  method  : 'POST',
                  data :  { _token : '{{ csrf_token() }}', impression : index , offer : {{ $offer->id }} },
                  success : function (data) {
                      toastr.info('Votre impression a été enregistrer avec succés.')
                  },
                  failed : function (err) {
                      console.error(err)
                  },
                  error : function (err) {
                      console.error(err)
                  }
              })
            first = true
        }
        var map;
        function initMap() {
            var location = {lat: {{ explode('|' , $offer->item->location)[0] }}, lng: {{ explode('|' , $offer->item->location)[1] }} };

            map = new google.maps.Map(document.getElementById('map'), {
                center: location,
                zoom: 7
            });
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            gmarkers.push(marker);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBheVz-S_cHHNu_ZQWlCKGJKXOK1-s13k8&callback=initMap"
            async defer></script>
@endsection
@section('script')
    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js') }}"></script>
@endsection
