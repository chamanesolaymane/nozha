@php $link = 'offers';@endphp
@extends('admin.layout')
@section('title') New Offer @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <button form="store" class="btn btn-primary btn-sm mr-2"><i class="fa fa-save"></i></button>
    </div>
    <form action="{{ route('admin.offers.store') }}" method="post" id="store" enctype="multipart/form-data">
        @csrf
        <div class="card mt-2">
            <div class="card-header">
                <h5 class="card-title">The offer</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>The Item</label>
                            <select name="item" class="form-control select2bs4 @error('item') is-invalid @enderror" style="width: 100%;">
                                <option selected disabled>Item</option>
                                @foreach($items as $item)
                                    <option {{ old('item') == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('item') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Title:</label>
                            <input type="text" name="title" value="{{ old('title') }}" class="form-control @error('title') is-invalid @enderror">
                            @error('title') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Price MAD:</label>
                            <input type="number" name="price" value="{{ old('price') }}" class="form-control @error('price') is-invalid @enderror">
                            @error('price') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Discount %:</label>
                            <input type="number" name="discount" value="{{ old('discount') ?? 0 }}" class="form-control @error('discount') is-invalid @enderror">
                            @error('discount') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cover</label>
                            <input type="file" name="cover" class="form-control">
                            @error('cover') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Offer</label>
                            <textarea class="offer @error('offer') is-invalid @enderror" name="offer"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('offer') }}</textarea>
                            @error('offer') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="description @error('description') is-invalid @enderror" name="description"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('description') }}</textarea>
                            @error('description') <small class="text-danger">{{ $message }}</small> @enderror

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Video:</label>
                            <input type="text" name="video" value="{{ old('video') }}" class="form-control @error('video') is-invalid @enderror">
                            @error('video') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Video title:</label>
                            <input type="text" name="video_title" value="{{ old('video_title') }}" class="form-control @error('video_title') is-invalid @enderror">
                            @error('video_title') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
    @endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Offer</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">{{ config('app.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.items.index') }}">Offers</a></li>
                        <li class="breadcrumb-item">New Offer</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
@endsection
@section('js')
    <script>
        $(function () {
            // Summernote
            $('.offer').summernote()
            $('.description').summernote()
        })
    </script>
    @endsection
@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">
@endsection
