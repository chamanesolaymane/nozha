@php $link = 'offers';@endphp
@extends('admin.layout')
@section('title') Offer {{ $offer->title }} @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <a href="{{ route('admin.offers.edit' , ['offer' => $offer->id]) }}" class="btn btn-sm btn-primary mr-2"><i class="fa fa-edit"></i></a>
        <a href="{{ route('admin.offers.activate' , ['id' => $offer->id]) }}" class="btn btn-sm btn-{{ $offer->active ? 'warning' : 'success' }} mr-2">
            <i class="fa {{ $offer->active ? 'fa-stop-circle' : 'fa-check' }}"></i>
        </a>
        <a href="{{ route('admin.offers.sponsor' , ['id' => $offer->id]) }}" class="btn btn-sm btn-{{ $offer->sponsored ? 'light' : 'success' }} mr-2">
            <i class="fa fa-star"></i>
        </a>
        <button class="btn btn-danger btn-sm" onclick="if(confirm('are you sure')) $('#delete').submit()">
            <i class="fa fa-trash"></i>
            <form action="{{ route('admin.offers.destroy' , ['offer' => $offer->id]) }}" id="delete" method="post">@csrf @method('DELETE')</form>
        </button>
    </div>
    <div class="card mt-2">
        <div class="card-header">
            <h5 class="card-title">The offer {{ $offer->title }} (@for($i = 0; $i < ($offer->impressions()->count() != 0 ? $offer->impressions()->avg('impression') : 0); $i++)
                    <i class="fa fa-star text-warning"></i>
                @endfor
                @if($offer->impressions()->count() == 0)
                    <i class="fa fa-star text-warning"></i>
                @endif
)</h5> &nbsp;(Reservations {{ $offer->reservations()->count() }})
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>The Item</label>
                        <input type="text" value="{{ $offer->item->name }}" class="form-control" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Title:</label>
                        <input type="text" name="title" readonly value="{{ $offer->title }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Price MAD:</label>
                        <input type="number" name="price" readonly value="{{ $offer->price }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Discount %:</label>
                        <input type="number" name="discount" readonly value="{{ $offer->discount ?? 0 }}" class="form-control ">
                    </div>
                </div>
                <div class="col-md-6 d-flex align-self-center justify-content-center">
                    @if($offer->cover == null)<h4 class="text-center text-muted">Cover not available</h4> @else
                        <a href="{{ asset('offers/covers/'.$offer->cover) }}" data-toggle="lightbox" data-title="The cover" data-gallery="gallery">
                            <img src="{{ asset('offers/covers/'.$offer->cover) }}" class="img-fluid mb-2" />
                        </a>@endif
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Offer</label><br>
                        {!! $offer->offer !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Description</label>
                        {!! $offer->description !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Video:</label>
                        <input type="text" name="video" value="{{ $offer->video }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Video title:</label>
                        <input type="text" name="video_title" value="{{ $offer->video_title }}" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Offer {{ $offer->title }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">{{ config('app.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.items.index') }}">Offer</a></li>
                        <li class="breadcrumb-item">{{ $offer->title }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js') }}"></script>
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('update')) toastr.info('You have been update the item {{ session()->get('update') }}.') @endif
            @if(session()->has('activate')) toastr.success('You have been activated the item {{ session()->get('activate') }}.') @endif
            @if(session()->has('deactivate')) toastr.warning('You have been deactivate the item {{ session()->get('deactivate') }}.')  @endif
            @if(session()->has('sponsor')) toastr.success('You have been sponsored the item {{ session()->get('sponsor') }}.') @endif
            @if(session()->has('no_sponsor')) toastr.warning('You have been not sponsored the item {{ session()->get('no_sponsor') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
        $(function () {
            $(document).on('click', '[data-toggle="gallery"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            $('.filter-container').filterizr({gutterPixels: 3});
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })

    </script>
    <script>
        $(function () {
            // Summernote
            $('.offer').summernote()
            $('.description').summernote()
        })
    </script>

@endsection
@section('style')  <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">

@endsection
