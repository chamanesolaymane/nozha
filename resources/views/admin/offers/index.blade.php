@php $link = 'offers'; @endphp

@extends('admin.layout')

@section('title') Offers @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <a href="{{ route('admin.offers.create') }}" class="btn btn-primary btn-sm mr-2"><i class="fa fa-plus-square"></i></a>
        <a href="" class="btn btn-info btn-sm"><i class="fa fa-chart-area"></i></a>
    </div>
    <div class="card mt-2">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Item</th>
                    <th>Price MAD</th>
                    <th>Discount %</th>
                    <th>Status</th>
                    <th>Sponsored</th>
                    <th>Created at</th>
                    <th>Last update</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($offers as $offer)
                    <tr>
                        <td>{{ $offer->title }}</td>
                        <td class="text-capitalize">{{ $offer->item->name }}</td>
                        <td>{{ $offer->price }}</td>
                        <td>{{ $offer->discount }}</td>
                        <td>
                            @if($offer->active) <span class="badge badge-success">Active</span> @else
                                <span class="badge badge-danger">Deactivate</span>
                            @endif
                        </td>
                        <td>
                            @if($offer->sponsored) <span class="badge badge-success">Sponsored</span> @else
                                <span class="badge badge-light">No sponsored</span>
                            @endif
                        </td>
                        <td>{{ date('d/m/Y' , strtotime($offer->created_at)) }}</td>
                        <td>{{ date('d/m/Y' , strtotime($offer->updated_at)) }}</td>
                        <td>
                            <div class="dropdown d-flex justify-content-center">
                                <a class="nav-link" data-toggle="dropdown">
                                    <i class="fa fa-list"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-items dropdown-menu-right text-center">
                                    <a href="{{ route('admin.offers.show' , ['offer' => $offer->id]) }}" class="dropdown-item">
                                        <i class="fas fa-info mr-2"></i> Infos
                                    </a>
                                    <a href="{{ route('admin.offers.edit' , ['offer' => $offer->id]) }}" class="dropdown-item">
                                        <i class="fas fa-edit mr-2"></i> Edit
                                    </a>
                                    <a href="{{ route('admin.offers.activate' , ['id' => $offer->id]) }}" class="dropdown-item">
                                        <i class="fas {{ $offer->active ? 'fa-stop-circle' : 'fa-check' }}"></i> {{ $offer->active ? 'Deactivate' : 'Active' }}
                                    </a>
                                    <a href="{{ route('admin.offers.sponsor' , ['id' => $offer->id]) }}" class="dropdown-item">
                                        <i class="fas fa-star"></i> {{ $offer->sponsored ? 'No sponsor' : 'Sponsor' }}
                                    </a>
                                    <button form="#delete" onclick="if( confirm('are you sure')) $('#delete').submit()" class="dropdown-item">
                                        <i class="fas fa-trash mr-2"></i> Delete
                                        <form action="{{ route('admin.offers.destroy' , ['offer' => $offer->id]) }}" method="post" id="delete">@csrf @method('DELETE')</form>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Offers</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.offers.index') }}">Offers</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

@endsection

@section('script')
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('add')) toastr.success('You have been added the offer {{ session()->get('add') }}.') @endif
            @if(session()->has('destroy')) toastr.warning('You have been deleted the offer {{ session()->get('destroy') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection

@section('style')

@endsection
