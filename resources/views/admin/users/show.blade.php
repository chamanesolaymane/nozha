@php $link = 'users'; @endphp

@extends('admin.layout')

@section('title') Users {{ $user->full_name }} @endsection

@section('content')
    <div class="card mt-2">
        <div class="card-header">
            <div class="card-title">
                User {{ $user->full_name }}
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" value="{{ $user->full_name }}" readonly class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" value="{{ $user->email }}" readonly class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" value="{{ $user->phone }}" readonly class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <label>Réservations:</label>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Offer</th>
                            <th>Date</th>
                            <th>Members</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->reservations()->orderByDesc('id')->get() as $reservation)
                            <tr class="{{ !$reservation->discovered ? 'text-bold' : '' }}">
                                <td>{{ $reservation->id }}</td>
                                <td>{{ $reservation->offer->item->name }}</td>
                                <td>{{ $reservation->offer->title}}</td>
                                <td>{{ date('Y-m-d' , strtotime($reservation->date))}}</td>
                                <td>{{ $reservation->members }}</td>
                                <td>{{ date('Y-m-d' , strtotime($reservation->created_at))}}</td>
                                <td>
                                    <div class="dropdown d-flex justify-content-center">
                                        <a class="nav-link" data-toggle="dropdown">
                                            <i class="fa fa-list"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-items dropdown-menu-right text-center">
                                            <a href="" class="dropdown-item">
                                                <i class="fas {{ $reservation->discovered ? 'fa-file' : 'fa-check' }} mr-2"></i> {{ $reservation->discovered ? 'Mark as unread' : 'Mark as read' }}
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.users.show' , ['user' => $user->id]) }}">{{ $user->name }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

@endsection

@section('script')
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('add')) toastr.success('You have been added the offer {{ session()->get('add') }}.') @endif
            @if(session()->has('destroy')) toastr.warning('You have been deleted the offer {{ session()->get('destroy') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection

@section('style')

@endsection
