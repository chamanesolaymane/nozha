@php $link = 'users'; @endphp

@extends('admin.layout')

@section('title') Users @endsection

@section('content')

    <div class="card mt-2">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Réservations</th>
                    <th>Created at</th>
                    <th>Last update</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td class="text-capitalize">{{ $user->full_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->reservations()->count() }}</td>
                        <td>{{ date('d/m/Y' , strtotime($user->created_at)) }}</td>
                        <td>{{ date('d/m/Y' , strtotime($user->updated_at)) }}</td>
                        <td>
                            <div class="dropdown d-flex justify-content-center">
                                <a class="nav-link" data-toggle="dropdown">
                                    <i class="fa fa-list"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-items dropdown-menu-right text-center">
                                    <a href="{{ route('admin.users.show' , ['user' => $user->id]) }}" class="dropdown-item">
                                        <i class="fas fa-info mr-2"></i> Infos
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Offers</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

@endsection

@section('script')
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('add')) toastr.success('You have been added the offer {{ session()->get('add') }}.') @endif
            @if(session()->has('destroy')) toastr.warning('You have been deleted the offer {{ session()->get('destroy') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection

@section('style')

@endsection
