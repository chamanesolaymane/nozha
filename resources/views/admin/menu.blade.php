<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar nav-legacy flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('admin.dashboard') }}" class="nav-link {{ $link == 'dashboard' ? 'active' : '' }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.items.index') }}" class="nav-link {{ $link == 'items' ? 'active' : '' }}">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Items
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.users.index') }}" class="nav-link {{ $link == 'users' ? 'active' : '' }}">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Users
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.offers.index') }}" class="nav-link {{ $link == 'offers' ? 'active' : '' }}">
                <i class="nav-icon fas fa-file"></i>
                <p>
                    Offers
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.reservations.index') }}" class="nav-link {{ $link == 'reservations' ? 'active' : '' }}">
                <i class="nav-icon fas fa-file"></i>
                <p>
                    Réservations <span class="badge badge-info right">{{ \App\Entries\Reservation::where('discovered' , false)->count() }}</span>
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.settings.index') }}" class="nav-link {{ $link == 'settings' ? 'active' : '' }}">
                <i class="nav-icon fas fa-cogs"></i>
                <p>
                    Paramètres
                </p>
            </a>
        </li>

    </ul>
</nav>
<!-- /.sidebar-menu -->
