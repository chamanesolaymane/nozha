@php $link = 'reservations'; @endphp

@extends('admin.layout')

@section('title') Réservations @endsection

@section('content')
    <div class="card mt-2">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Offer</th>
                    <th>Date</th>
                    <th>Members</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($reservations as $reservation)
                    <tr class="{{ !$reservation->discovered ? 'text-bold' : '' }}">
                        <td>{{ $reservation->id }}</td>
                        <td>{{ $reservation->offer->item->name }}</td>
                        <td>{{ $reservation->offer->title}}</td>
                        <td>{{ date('Y-m-d' , strtotime($reservation->date))}}</td>
                        <td>{{ $reservation->members }}</td>
                        <td>{{ date('Y-m-d' , strtotime($reservation->created_at))}}</td>
                        <td>
                            <div class="dropdown d-flex justify-content-center">
                                <a class="nav-link" data-toggle="dropdown">
                                    <i class="fa fa-list"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-items dropdown-menu-right text-center">
                                    <a href="{{ route('admin.reservations.read' , ['id' => $reservation->id]) }}" class="dropdown-item">
                                        <i class="fas {{ $reservation->discovered ? 'fa-file' : 'fa-check' }} mr-2"></i> {{ $reservation->discovered ? 'Mark as unread' : 'Mark as read' }}
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Items</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.reservations.index') }}">Réservations</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

@endsection

@section('script')
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('add')) toastr.info('You have been added the item {{ session()->get('update') }}.') @endif
            @if(session()->has('destroy')) toastr.warning('You have been deleted the item {{ session()->get('destroy') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection

@section('style')

@endsection
