    @php $link = 'items';@endphp
    @extends('admin.layout')
    @section('title') New Item @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <button form="store" class="btn btn-primary btn-sm mr-2"><i class="fa fa-save"></i></button>
    </div>
    <form action="{{ route('admin.items.store') }}" method="post" id="store" enctype="multipart/form-data">
        @csrf
        <div class="card mt-2">
            <div class="card-header">
                <h5 class="card-title">General</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror">
                            @error('name') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>City</label>
                            <select name="city" class="form-control select2bs4 @error('city') is-invalid @enderror" style="width: 100%;">
                                <option selected disabled>City</option>
                                <option {{ old('city') == 'casa' ? 'selected' : '' }} value="casa"> Casablanca</option>
                                <option {{ old('city') == 'marrakech' ? 'selected' : '' }} value="marrakech"> Marrakech</option>
                                <option {{ old('city') == 'rabat' ? 'selected' : '' }} value="rabat"> Rabat</option>
                                <option {{ old('city') == 'tangier' ? 'selected' : '' }} value="tangier"> Tanger</option>
                                <option {{ old('city') == 'agadir' ? 'selected' : '' }} value="agadir"> Agadir</option>
                                <option {{ old('city') == 'fes' ? 'selected' : '' }} value="fes"> Fes</option>
                                <option {{ old('city') == 'ouarzazate' ? 'selected' : '' }} value="ouarzazate"> Ouarzazate</option>
                                <option {{ old('city') == 'essaouira' ? 'selected' : '' }} value="essaouira"> Essaouira</option>
                            </select>
                            @error('city') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Type</label>
                            <select name="type" class="form-control select2bs4 @error('type') is-invalid @enderror" style="width: 100%;">
                                <option selected disabled>Type</option>
                                <option {{ old('type') == 'activities' ? 'selected' : '' }} value="activities"> Activité</option>
                                <option {{ old('type') == 'restaurants' ? 'selected' : '' }} value="restaurants"> Restaurant</option>
                                <option {{ old('type') == 'hotels' ? 'selected' : '' }} value="hotels"> Hôtel</option>
                                <option {{ old('type') == 'health' ? 'selected' : '' }} value="coffees"> Cafés</option>
                                <option {{ old('type') == 'health' ? 'selected' : '' }} value="health"> Santé & SPA</option>
                            </select>
                            @error('type') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Address</label>
                            <textarea name="address" class="form-control @error('address') is-invalid @enderror" rows="5" style="resize: none">{{ old('address') }}</textarea>
                            @error('address') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Avatar</label>
                            <input type="file" name="avatar" class="form-control">
                            @error('avatar') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cover</label>
                            <input type="file" name="cover" class="form-control">
                            @error('cover') <small class="text-danger">{{ $message }}</small> @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-2">
            <div class="card-header">
                <h5>Geolocation</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="map" class="d-block" style="width: 100%; height: 540px"></div>
                        <input type="hidden" name="location" id="location" value="{{ old('location') }}">
                    </div>
                    <div class="col-md-6 mt-2">
                        <label>Latitude</label>
                        <input type="text" readonly class="form-control" id="lat" value="{{ explode('|' , old('location'))[0] ?? '' }}">
                    </div>
                    <div class="col-md-6 mt-2">
                        <label>Longitude</label>
                        <input type="text" readonly class="form-control" id="lng" value="{{ explode('|' , old('location'))[1] ?? '' }}">
                    </div>
                </div>
                @error('location') <small class="text-danger text-center">{{ $message }}</small> @enderror
            </div>
        </div>
    </form>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New City</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">{{ config('app.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.items.index') }}">Items</a></li>
                        <li class="breadcrumb-item">New Item</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
        var map;
        function initMap() {
                @if($errors->any() && old('location') != null) var location = { lat : {{ explode('|',old('location'))[0] }} , lng : {{ explode('|',old('location'))[1] }} };
                @else var location = {lat: 30.9431046, lng: -6.9752925 };  @endif

                map = new google.maps.Map(document.getElementById('map'), {
                center: location,
                zoom: 8
            });
            google.maps.event.addListener(map, 'click', function(event) {
                removeMarkers()
                placeMarker(event.latLng);
                $('#location').val(event.latLng.lat() +'|' + event.latLng.lng() )
                $('#lat').val(event.latLng.lat())
                $('#lng').val(event.latLng.lng())
            });
            var gmarkers = [];
            function placeMarker(location) {
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                gmarkers.push(marker);
            }
            function removeMarkers(){
                for(i=0; i<gmarkers.length; i++){
                    gmarkers[i].setMap(null);
                }
            }
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBheVz-S_cHHNu_ZQWlCKGJKXOK1-s13k8&callback=initMap"
            async defer></script>
@endsection
@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection
