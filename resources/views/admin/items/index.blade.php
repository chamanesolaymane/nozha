@php $link = 'items'; @endphp

@extends('admin.layout')

@section('title') Items @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <a href="{{ route('admin.items.create') }}" class="btn btn-primary btn-sm mr-2"><i class="fa fa-plus-square"></i></a>
        <a href="" class="btn btn-info btn-sm"><i class="fa fa-chart-area"></i></a>
    </div>
    <div class="card mt-2">
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>created at</th>
                    <th>last update</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td class="text-capitalize">{{ $item->type }}</td>
                        <td>{{ $item->city }}</td>
                        <td>{{ $item->address }}</td>
                        <td>
                            @if($item->active) <span class="badge badge-success">Active</span> @else
                                <span class="badge badge-danger">Deactivate</span>
                            @endif
                        </td>
                        <td>{{ date('d/m/Y' , strtotime($item->created_at)) }}</td>
                        <td>{{ date('d/m/Y' , strtotime($item->updated_at)) }}</td>
                        <td>
                            <div class="dropdown d-flex justify-content-center">
                                <a class="nav-link" data-toggle="dropdown">
                                    <i class="fa fa-list"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-items dropdown-menu-right text-center">
                                    <a href="{{ route('admin.items.show' , ['item' => $item->id]) }}" class="dropdown-item">
                                        <i class="fas fa-info mr-2"></i> Infos
                                    </a>
                                        <a href="{{ route('admin.items.edit' , ['item' => $item->id]) }}" class="dropdown-item">
                                            <i class="fas fa-edit mr-2"></i> Edit
                                        </a>
                                        <a href="{{ route('admin.items.activate' , ['id' => $item->id]) }}" class="dropdown-item">
                                            <i class="fas {{ $item->active ? 'fa-stop-circle' : 'fa-check' }}"></i> {{ $item->active ? 'Deactivate' : 'Active' }}
                                        </a>
                                        <button form="#delete" onclick="if( confirm('are you sure')) $('#delete').submit()" class="dropdown-item">
                                            <i class="fas fa-trash mr-2"></i> Delete
                                            <form action="{{ route('admin.items.destroy' , ['item' => $item->id]) }}" method="post" id="delete">@csrf @method('DELETE')</form>
                                        </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Items</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">{{ config('app.name') }}</li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.items.index') }}">Items</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

@endsection

@section('script')
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    <script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('add')) toastr.info('You have been added the item {{ session()->get('update') }}.') @endif
            @if(session()->has('destroy')) toastr.warning('You have been deleted the item {{ session()->get('destroy') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection

@section('style')

@endsection
