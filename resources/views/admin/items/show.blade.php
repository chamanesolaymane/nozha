@php $link = 'items';@endphp
@extends('admin.layout')
@section('title') Item {{ $item->name }} @endsection

@section('content')
    <div class="d-flex justify-content-end">
        <a href="{{ route('admin.items.edit' , ['item' => $item->id]) }}" class="btn btn-sm btn-primary mr-2"><i class="fa fa-edit"></i></a>
        <a href="{{ route('admin.items.activate' , ['id' => $item->id]) }}" class="btn btn-sm btn-{{ $item->active ? 'warning' : 'success' }} mr-2">
            <i class="fa {{ $item->active ? 'fa-stop-circle' : 'fa-check' }}"></i>
        </a>
        <button class="btn btn-danger btn-sm" onclick="if(confirm('are you sure')) $('#delete').submit()">
            <i class="fa fa-trash"></i>
            <form action="{{ route('admin.items.destroy' , ['item' => $item->id]) }}" id="delete" method="post">@csrf @method('DELETE')</form>
        </button>
    </div>
    <div class="card mt-2">
        <div class="card-header">
            <h5 class="card-title">General</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" readonly name="name" value="{{ $item->name }}" class="form-control @error('name') is-invalid @enderror">
                        @error('name') <small class="text-danger">{{ $message }}</small> @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>City</label>
                        <select name="city" disabled class="form-control select2bs4 @error('city') is-invalid @enderror" style="width: 100%;">
                            <option selected disabled>City</option>
                            <option {{ $item->city == 'casa' ? 'selected' : '' }} value="casa"> Casablanca</option>
                            <option {{ $item->city == 'marrakech' ? 'selected' : '' }} value="marrakech"> Marrakech</option>
                            <option {{ $item->city == 'rabat' ? 'selected' : '' }} value="rabat"> Rabat</option>
                            <option {{ $item->city == 'tangier' ? 'selected' : '' }} value="tangier"> Tanger</option>
                            <option {{ $item->city == 'agadir' ? 'selected' : '' }} value="tangier"> Agadir</option>
                            <option {{ $item->city == 'fes' ? 'selected' : '' }} value="tangier"> Fes</option>
                            <option {{ $item->city == 'ouarzazate' ? 'selected' : '' }} value="tangier"> Ouarzazate</option>
                            <option {{ $item->city == 'essaouira' ? 'selected' : '' }} value="tangier"> Essaouira</option>
                        </select>
                        @error('city') <small class="text-danger">{{ $message }}</small> @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" disabled class="form-control select2bs4 @error('type') is-invalid @enderror" style="width: 100%;">
                            <option {{ $item->type == 'activities' ? 'selected' : '' }} value="activities"> Activité</option>
                            <option {{ $item->type == 'restaurants' ? 'selected' : '' }} value="restaurants"> Restaurant</option>
                            <option {{ $item->type == 'hotels' ? 'selected' : '' }} value="hotels"> Hôtel</option>
                            <option {{ $item->type == 'remains' ? 'selected' : '' }} value="remains"> Séjour</option>
                            <option {{ $item->type == 'ferry' ? 'selected' : '' }} value="ferry"> Ferry</option>
                        </select>
                        @error('type') <small class="text-danger">{{ $message }}</small> @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="address" readonly class="form-control @error('address') is-invalid @enderror" rows="5" style="resize: none">{{ $item->address }}</textarea>
                        @error('address') <small class="text-danger">{{ $message }}</small> @enderror
                    </div>
                </div>
                <div class="col-md-6 d-flex align-self-center justify-content-center">
                    <a href="{{ asset('items/avatars/'.$item->avatar) }}"  data-title="The avatar" >
                        <img src="{{ asset('items/avatars/'.$item->avatar) }}" class="img-fluid mb-2" alt="white sample"/>
                    </a>
                </div>
                <div class="col-md-6 d-flex align-self-center justify-content-center">
                    @if($item->cover == null)<h4 class="text-center text-muted">Cover not available</h4> @else
                        <a href="{{ asset('items/covers/'.$item->cover) }}" data-toggle="lightbox" data-title="The cover" data-gallery="gallery">
                            <img src="{{ asset('items/covers/'.$item->cover) }}" class="img-fluid mb-2" />
                        </a>@endif
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header">
            <h5>Geolocation</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="map" class="d-block" style="width: 100%; height: 540px"></div>
                    <input type="hidden" name="location" id="location" value="{{ old('location') }}">
                </div>
                <div class="col-md-6 mt-2">
                    <label>Latitude</label>
                    <input type="text" readonly class="form-control" id="lat" value="{{ explode('|' , $item->location)[0] }}">
                </div>
                <div class="col-md-6 mt-2">
                    <label>Longitude</label>
                    <input type="text" readonly class="form-control" id="lng" value="{{ explode('|' , $item->location)[1] }}">
                </div>
            </div>
            @error('location') <small class="text-danger text-center">{{ $message }}</small> @enderror
        </div>
    </div>
    @endsection

@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Item {{ $item->name }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">{{ config('app.name') }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.items.index') }}">Items</a></li>
                        <li class="breadcrumb-item">{{ $item->name }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
@endsection

@section('script')
    <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js') }}"></script>
@endsection

@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
                "ordering": false,
            });
            @if(session()->has('update')) toastr.info('You have been update the item {{ session()->get('update') }}.') @endif
            @if(session()->has('activate')) toastr.success('You have been activated the item {{ session()->get('activate') }}.') @endif
            @if(session()->has('deactivate')) toastr.warning('You have been deactivate the item {{ session()->get('deactivate') }}.')  @endif
        });
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
        $(function () {
            $(document).on('click', '[data-toggle="gallery"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            $('.filter-container').filterizr({gutterPixels: 3});
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })
        var map;
        function initMap() {
            var location = {lat: {{ explode('|' , $item->location)[0] }}, lng: {{ explode('|' , $item->location)[1] }} };

            map = new google.maps.Map(document.getElementById('map'), {
                center: location,
                zoom: 8
            });
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            gmarkers.push(marker);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBheVz-S_cHHNu_ZQWlCKGJKXOK1-s13k8&callback=initMap"
            async defer></script>

@endsection
@section('style')  <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}"> @endsection
