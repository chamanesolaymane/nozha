<?php

use Illuminate\Support\Facades\Route;

Route::view('dashboard' , 'clients.dashboard.index')->name('dashboard');
Route::resource('reservations' , 'ReservationsController');
Route::resource('settings' , 'SettingsController')->only('index' , 'update');
