<?php

use Illuminate\Support\Facades\Route;

Route::middleware('admin.guest')->group(function (){
    Route::get('/' , 'LoginController@form')->name('login.form');
    Route::post('/' , 'LoginController@attempt')->name('login');
});

Route::middleware('admin.connected')->group(function (){
    Route::get('dashboard' , 'DashboardController@index')->name('dashboard');

    Route::get('logout' , function (){
        \Illuminate\Support\Facades\Auth::guard('admin')->logout();
        return redirect()->route('admin.login.form');
    })->name('logout');
    Route::resources([
        'items' => 'ItemsController',
        'offers' => 'OffersController',
        'users' => 'UsersController',
        'reservations' => 'ReservationsController'
    ]);
    Route::resource('settings' , 'SettingsController')->only('index' , 'update');
    Route::get('/items/activate/{id}' , 'ItemsController@activate')->name('items.activate');
    Route::get('/offers/activate/{id}' , 'OffersController@activate')->name('offers.activate');
    Route::get('/offers/sponsor/{id}' , 'OffersController@sponsored')->name('offers.sponsor');
    Route::get('/reservations/read/{id}' , 'ReservationsController@toggleRead')->name('reservations.read');
});
