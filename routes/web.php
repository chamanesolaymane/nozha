<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Landing')->group(function (){
    Route::get('/','OffersController@index')->name('landing.index');
    Route::get('offers/{type?}','OffersController@index')->name('landing.type');
    Route::group(['prefix' => 'offers' ,'as' => 'landing.'] , function (){
        Route::get('show/{id}' , 'OffersController@show')->name('show');
        Route::post('search' , 'OffersController@search')->name('search');
        Route::post('query' , 'OffersController@query')->name('query');
    });
    Route::resource('comments' , 'CommentsController')->only('store');
    Route::resource('impressions' , 'ImpressionsController')->only('store');
});

Route::middleware('guest') ->group(function (){
    Route::view('login' , 'clients.login')->name('login.form');
    Route::post('login' , 'Auth\LoginController@attempt')->name('login');
    Route::view('register' , 'clients.register')->name('register.form');
    Route::post('register' , 'Auth\RegisterController@attempt')->name('register');
});
Route::get('logout' , function (){
    \Illuminate\Support\Facades\Auth::logout();
    return redirect('/');
})->name('logout');
