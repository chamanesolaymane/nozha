<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Entries\Item::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'type' => $faker->randomElement(['activities' , 'restaurants' , 'hotels' , 'remains' , 'ferry']),
        'city' => $faker->city,
        'location' => $faker->latitude.'|'.$faker->longitude,
        'address' => $faker->address,
        'active' => $faker->boolean,
        'avatar' => $faker->imageUrl(),
        'cover' => $faker->imageUrl()
    ];
});


