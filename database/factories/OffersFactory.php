<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Entries\Offer::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'cover' => $faker->imageUrl(),
        'price' => $faker->numberBetween(0,2000),
        'discount' => $faker->numberBetween(0,40),
        'offer' => $faker->paragraph,
        'description' => $faker->paragraph,
        'sponsored'=> $faker->boolean
    ];
});
