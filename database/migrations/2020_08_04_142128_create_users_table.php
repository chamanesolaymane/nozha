<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->ipAddress('ip')->nullable();
            $table->string('phone');
            $table->rememberToken();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        \App\User::create([
            'first_name' => 'client',
            'last_name' => 'traveler',
            'email' => 'client@mail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('123456789'),
            'ip' => '127.0.0.1',
            'phone' => '0614702844'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
