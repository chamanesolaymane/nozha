<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('cover')->nullable();
            $table->double('price');
            $table->double('discount')->default(0);
            $table->longText('offer')->nullable();
            $table->longText('description')->nullable();
            $table->string('video')->nullable();
            $table->string('video_title')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('sponsored')->default(false);
            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items')->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });

        /*factory(\App\Entries\Item::class , 20)->create()->each(function ($item){
            $item->offers()->createMany(factory(\App\Entries\Offer::class , 10)->make()->toArray());
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
