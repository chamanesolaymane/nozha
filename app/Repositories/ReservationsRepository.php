<?php


namespace App\Repositories;


use App\Contracts\IOffers;
use App\Contracts\IReservations;
use App\Entries\Reservation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class ReservationsRepository implements IReservations
{
    private $offers;

    public function __construct(IOffers $offers)
    {
        $this->offers = $offers;
    }

    public function list(): Collection
    {
        // TODO: Implement list() method.
        return Reservation::orderByDesc('id')->get();
    }

    public function create(array $data): Reservation
    {
        // TODO: Implement create() method.
        $reservation = $this->offers->find($data['offer'])->reservations()->create([
            'date' => $data['date'],
            'members' => $data['members'],
            'user_id' => Auth::id()
        ]);
        session()->flash('add' , $reservation->id);
        return $reservation;
    }

    public function update(array $data): void
    {
        // TODO: Implement update() method.
    }

    public function find(int $id): Reservation
    {
        // TODO: Implement find() method.
        return Reservation::all()->find($id) ?? abort(404);
    }

    public function delete(int $id): void
    {
        // TODO: Implement delete() method.
    }

    public function listByCurrentUser(): Collection
    {
        // TODO: Implement listByCurrentUser() method.
        return Auth::user()->reservations()->orderByDesc('id')->get();
    }

    public function toggleRead(int $id): void
    {
        // TODO: Implement toggleRead() method.
        $reservation = $this->find($id);
        $reservation->update(['discovered' => !$reservation->discovered]);
    }

}
