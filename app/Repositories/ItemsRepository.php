<?php


namespace App\Repositories;


use App\Contracts\IItems;
use App\Entries\Item;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class ItemsRepository implements IItems
{

    public function list(): Collection
    {
        // TODO: Implement list() method.
        return Item::orderByDesc('id')->get();
    }

    public function create(array $data, UploadedFile $avatar = null, UploadedFile $cover = null): Item
    {
        // TODO: Implement create() method.
        $item = Item::create([
            'name' => $data['name'],
            'type' => $data['type'],
            'location' => $data['location'],
            'address' => $data['address'],
            'city' => $data['city']
        ]);
        if ($avatar){
            $avatar->storeAs('items/avatars/' , $item->id.'.'.$avatar->getClientOriginalExtension());
            $item->avatar =$item->id.'.'. $avatar->getClientOriginalExtension();
        }
        if ($cover)
        {
            $cover->storeAs('items/covers/',$item->id.'.'. $cover->getClientOriginalExtension());
            $item->cover = $item->id.'.'.  $cover->getClientOriginalExtension();
        }
        $item->save();
        session()->flash('add' , $item->name);
        return $item;
    }

    public function update(int $id, array $data, UploadedFile $avatar = null, UploadedFile $cover = null): Item
    {
        // TODO: Implement update() method.
        $item = $this->find($id);
        $item->update([
            'name' => $data['name'],
            'type' => $data['type'],
            'location' => $data['location'],
            'address' => $data['address'],
            'city' => $data['city']
        ]);
        if ($avatar){
            Storage::delete('items/avatars/'.$item->avatar);
            $avatar->storeAs('items/avatars/',$item->id.'.'.$avatar->getClientOriginalExtension());
            $item->avatar =$item->id.'.'. $avatar->getClientOriginalExtension();
        }
        if ($cover)
        {
            Storage::delete('items/covers/'.$item->cover);
            $cover->storeAs('items/covers/',$item->id.'.'. $cover->getClientOriginalExtension());
            $item->cover = $item->id.'.'.  $cover->getClientOriginalExtension();
        }
        $item->save();
        session()->flash('update' , $item->name);
        return $item;
    }

    public function find(int $id): Item
    {
        // TODO: Implement find() method.
        return Item::all()->find($id) ?? abort(404);
    }

    public function delete(int $id)
    {
        // TODO: Implement delete() method.
        $item = $this->find($id);
        $item->forceDelete();
        if ($item->avatar)
            Storage::delete('items/avatars/'.$item->avatar);
        if ($item->cover)
            Storage::delete('items/covers/'.$item->cover);
        session()->flash('destroy' , $item->name);
    }

    public function activate(int $id)
    {
        // TODO: Implement activate() method.
        $item = $this->find($id);
        $item->update(['active' => !$item->active]);
        session()->flash($item->active ? 'activate' : 'deactivate' , $item->name);
    }

}
