<?php


namespace App\Repositories;


use App\Contracts\IItems;
use App\Contracts\IOffers;
use App\Entries\Offer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class OffersRepository implements IOffers
{
    private $items;
    public function __construct(IItems $items)
    {
        $this->items = $items;
    }

    public function list(): Collection
    {
        // TODO: Implement list() method.
        return Offer::orderByDesc('id')->get();
    }

    public function create(array $data, UploadedFile $cover = null): void
    {
        // TODO: Implement create() method.
        $offer = $this->items->find($data['item'])->offers()->create([
            'title' => $data['title'],
            'price' => $data['price'],
            'discount' => $data['discount'],
            'offer' => $data['offer'],
            'description' => $data['description'],
            'video' => $data['video'],
            'video_title' => $data['video_title'],
        ]);
        if ($cover)
        {
            $cover->storeAs('offers/covers' , $offer->id . '.'.$cover->getClientOriginalExtension());
            $offer->cover = $offer->id . '.'.$cover->getClientOriginalExtension();
            $offer->save();
        }
        session()->flash('add' , $offer->title);
    }

    public function update(int $id, array $data, UploadedFile $cover = null)
    {
        // TODO: Implement update() method.
        $offer = $this->find($id);
        $offer->update([
            'title' => $data['title'],
            'price' => $data['price'],
            'discount' => $data['discount'],
            'offer' => $data['offer'],
            'description' => $data['description'],
            'video' => $data['video'],
            'video_title' => $data['video_title'],
            'item_id' => $data['item']
        ]);
        if ($cover)
        {
            Storage::delete('offers/covers/'.$offer->cover);
            $cover->storeAs('offers/covers' , $offer->id . '.'.$cover->getClientOriginalExtension());
            $offer->cover = $offer->id . '.'.$cover->getClientOriginalExtension();
            $offer->save();
        }
        session()->flash('update' , $offer->title);
    }

    public function find(int $id): Offer
    {
        // TODO: Implement find() method.
        return Offer::all()->find($id) ?? abort(404);
    }

    public function delete(int $id): void
    {
        // TODO: Implement delete() method.
        $offer = $this->find($id);
        $offer->forceDelete();
        if ($offer->cover)
            Storage::delete('offers/covers/'.$offer->cover);
        session('delete' , $offer->title);
    }

    public function activate(int $id): void
    {
        // TODO: Implement activate() method.
        $offer = $this->find($id);
        $offer->update(['active' => !$offer->active]);
        session()->flash($offer->active ? 'activate'  : 'deactivate' , $offer->title);
    }

    public function sponsored(int $id): void
    {
        // TODO: Implement sponsored() method.
        $offer = $this->find($id);
        $offer->update(['sponsored' => !$offer->sponsored]);
        session()->flash($offer->sponsored ? 'sponsor'  : 'no_sponsor' , $offer->title);
    }

    public function listByType(string $type)
    {
        // TODO: Implement listByType() method.
        if ($type == 'sponsored')
            return Offer::where('active' , true)->where('sponsored' , true)->orderByDesc('id')->paginate(24);
        else
            return Offer::where('offers.active' , true)->where('items.type' , $type)->join('items' , 'offers.item_id' , '=' , 'items.id')->orderByDesc('offers.id')->paginate(24);
    }

   public function search(Request $request)
   {
       // TODO: Implement search() method.
       return Offer::where('offers.active' , true)->where('items.city' , $request->city)
           ->join('items' , 'offers.item_id' , '=' , 'items.id')->orderByDesc('offers.id')->paginate(24);
   }

   public function comment(array $data): void
   {
       // TODO: Implement comment() method.
       $this->find($data['offer'])->comments()->create([
           'text' => $data['text'],
           'user_id' => Auth::id()
       ]);
   }

   public function impression(array $data): void
   {
       // TODO: Implement impression() method.
       $offer = $this->find($data['offer']);
       if ($offer->impressions()->count() != 0)
           $offer->impressions()->where('user_id' , Auth::id())->first()->update([
               'impression' => $data['impression']
           ]);
       else
           $offer->impressions()->create([
               'impression' => $data['impression'],
               'user_id' => Auth::id()
           ]);
   }

   public function offersByName(string $name)
   {
       // TODO: Implement offersByName() method.
       return Offer::where('offers.active' , true)->where('items.name' , 'like',  '%'.$name.'%')->orWhere('offers.title', 'like',  '%'.$name.'%')
           ->join('items' , 'offers.item_id' , '=' , 'items.id')->orderByDesc('offers.id')->paginate(24);
   }
}
