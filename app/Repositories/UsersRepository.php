<?php


namespace App\Repositories;


use App\Contracts\IUsers;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersRepository implements IUsers
{

    public function list(): Collection
    {
        // TODO: Implement list() method.
        return User::orderByDesc('id')->get();
    }

    public function create(array $data): User
    {
        // TODO: Implement create() method.
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'ip' => request()->ip(),
            'password' => Hash::make($data['password']),
            'phone' => phone($data['phone'] , $data['code'])
        ]);
    }

    public function update(int $id, array $data): void
    {
        // TODO: Implement update() method.
        $user = $this->find($id);
        $user->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email']
        ]);
        if (isset($data['password'])){
            $user->password = Hash::make($data['password']);
            $user->save();
        }
        Auth::logout();
    }

    public function find(int $id): User
    {
        // TODO: Implement find() method.
        return User::all()->find($id) ?? abort(404);
    }

    public function delete(int $id): void
    {
        // TODO: Implement delete() method.
        $user = $this->find($id);
        $user->forceDelete();
    }

    public function activate(int $id) : void {
        $user = $this->find($id);
        $user->update(['active' => !$user->active]);
        session()->flash('active' , $user->full_name);
    }
}
