<?php

namespace App\Http\Controllers\Landing;

use App\Contracts\IOffers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImpressionsController extends Controller
{
    private $offers;

    public function __construct(IOffers $offers)
    {
        $this->offers = $offers;
    }

    public function store(Request $request)
    {
        $this->offers->impression($request->all());
        return response()->json(['ok' => true]);
    }
}
