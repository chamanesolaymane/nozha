<?php

namespace App\Http\Controllers\Landing;

use App\Contracts\IOffers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    private $offers;

    public function __construct(IOffers $offers)
    {
        $this->offers = $offers;
    }

    public function store(Request $request){
        $this->validate($request , [
            'text' => 'required|string',
        ] ,
            [
                'text.required' => 'le champ commnetaire est obligatiore'
            ]);
        $this->offers->comment($request->all());
        return back();
    }
}
