<?php

namespace App\Http\Controllers\Landing;

use App\Contracts\IOffers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    private $offers;

    public function __construct(IOffers $offers)
    {
        $this->offers = $offers;
    }

    public function index($type = null){
        return view('landing.index' , ['offers' => $this->offers->listByType($type ?? 'sponsored') , 'type' => $type]);
    }

    public function show($id){
        return view('landing.show' , ['offer' => $this->offers->find($id)]);
    }

    public function search(Request $request){
        return view('landing.index' , ['offers' => $this->offers->search($request) , 'type' => null]);
    }

    public function query(Request $request){
        return view('landing.index' , ['offers' => $this->offers->offersByName($request->get('query')) , 'type' => null]);
    }
}
