<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\IUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    private $users;

    public function __construct(IUsers $users)
    {
        $this->users = $users;
    }

    public function form(){
        return view('clients.register');
    }

    public function attempt(Request $request){
        $this->validate($request , [
            'last_name' => 'required|string',
            'first_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'terms' => 'required',
            'phone' => 'required|unique:users'
        ]);
        $user = $this->users->create($request->all());
        Auth::login($user);
        return redirect()->route('landing.index');
    }
}
