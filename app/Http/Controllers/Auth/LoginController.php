<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function attempt(Request $request){
        $this->validate($request , [
            'email' => 'required|string|email:rfc,dns',
            'password' => 'required|string'
        ]);
        if (Auth::attempt(['email' => $request->email , 'password' => $request->password]))
            return redirect()->route('landing.index');
        else {
                session()->flash('failed' , trans('auth.failed'));
                return back();
        }
    }
}
