<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index(){
        return view('admin.settings.index' , ['user' => Auth::guard('admin')->user()]);
    }

    public function update($id ,Request $request){
        $this->validate($request , [
            'name' => 'required|string',
            'email' => 'required|string|email',
            'password' => 'nullable|string|confirmed',
        ]);

        $admin = Admin::all()->find(Auth::guard('admin')->id());
        $admin->update([
            'name' => $request->name,
            'password' => isset($request->password) ? Hash::make($request->password) : $admin->password ,
            'email' => $request->email
        ]);
        return isset($request->password) ? redirect()->route('admin.logout') : back();
    }
}
