<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function form(){
        return view('admin.login');
    }

    public function attempt(Request $request){
        $this->validate($request , [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);
        if (Auth::guard('admin')->attempt(['email' => $request->email , 'password' => $request->password]))
            return redirect()->route('admin.dashboard');
        else
            {
                session()->flash('failed' , trans('auth.failed'));
                return back();
            }
    }
}
