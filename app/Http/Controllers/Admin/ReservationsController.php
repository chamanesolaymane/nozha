<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\IReservations;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReservationsController extends Controller
{
    private $reservations;

    public function __construct(IReservations $reservations)
    {
        $this->reservations = $reservations;
    }

    public function index(){
        return view('admin.reservations.index' , ['reservations' => $this->reservations->list()]);
    }

    public function toggleRead(int $id){
        $this->reservations->toggleRead($id);
        return redirect()->route('admin.reservations.index');
    }
}
