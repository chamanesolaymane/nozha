<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\IItems;
use App\Http\Controllers\Controller;
use App\Http\Requests\Entries\ItemsRequest;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    private $items;

    public function __construct(IItems $items)
    {
        $this->items = $items;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.items.index' , ['items' => $this->items->list()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemsRequest $request)
    {
        $this->items->create($request->all() , $request->file('avatar') , $request->file('cover'));
        return redirect()->route('admin.items.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.items.show' , ['item' => $this->items->find($id) ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return  view('admin.items.edit' , ['item' => $this->items->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemsRequest $request, $id)
    {
        $this->items->update($id , $request->all() , $request->file('avatar') , $request->file('cover'));
        return redirect()->route('admin.items.show' , ['item' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->items->delete($id);
        return  redirect()->route('admin.items.index');
    }

    public function activate(int $id){
        $this->items->activate($id);
        return redirect()->route('admin.items.show' , ['item' => $id]);
    }
}
