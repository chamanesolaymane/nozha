<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\IItems;
use App\Contracts\IOffers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Entries\OffersRequest;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    private $offers;
    private $items;

    public function __construct(IItems $items , IOffers $offers)
    {
        $this->items = $items;
        $this->offers = $offers;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.offers.index' , ['offers' => $this->offers->list()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.offers.create' , ['items' => $this->items->list()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OffersRequest $request)
    {
        $this->offers->create($request->all() , $request->file('cover'));
        return redirect()->route('admin.offers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.offers.show' , ['offer' => $this->offers->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.offers.edit' , ['offer' => $this->offers->find($id) , 'items' => $this->items->list()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OffersRequest $request, $id)
    {
        $this->offers->update($id , $request->all() , $request->file('cover'));
        return redirect()->route('admin.offers.show' , ['offer' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->offers->delete($id);
        return  redirect()->route('admin.offers.index');
    }

    public function activate(int $id){
        $this->offers->activate($id);
        return redirect()->route('admin.offers.show' , ['offer' => $id]);
    }

    public function sponsored(int $id){
        $this->offers->sponsored($id);
        return redirect()->route('admin.offers.show' , ['offer' => $id]);
    }
}
