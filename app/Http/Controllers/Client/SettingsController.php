<?php

namespace App\Http\Controllers\Client;

use App\Contracts\IUsers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    private $users;

    public function __construct(IUsers $users)
    {
        $this->users = $users;
    }

    public function index(){
        return view('clients.settings.index' , ['user' => Auth::user()]);
    }

    public function update(Request $request){
        $this->users->update(Auth::id() , $request->all());
        return isset($request->password) ? redirect()->route('login.form') : back();
    }
}
