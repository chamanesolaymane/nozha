<?php

namespace App\Http\Requests\Entries;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OffersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'item' => 'required|numeric',
            'cover' => 'nullable|image',
            'offer' => 'required|string',
            'description' => 'required|string',
            'video' => 'nullable|url',
            'video_title' => 'nullable|string'
        ];
    }
}
