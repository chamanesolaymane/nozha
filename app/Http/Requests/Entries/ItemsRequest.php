<?php

namespace App\Http\Requests\Entries;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ItemsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => $this->method() == 'POST' ? 'required|string|unique:items' : ['required' , 'string' , Rule::unique('items')->ignore($this->item)],
            'avatar' => $this->method() == 'POST' ? 'required' : 'nullable' .'|image',
            'cover' => $this->method() == 'POST' ? 'required' : 'nullable' .'|image',
            'type' => 'required',
            'city' => 'required',
            'address' => 'required|string',
            'location' => 'required|string'
        ];
    }
}
