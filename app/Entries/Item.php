<?php

namespace App\Entries;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';

    protected $guarded = ['id'];

    protected $casts = [
      'active' => 'boolean'
    ];


    public function offers(){
        return $this->hasMany(Offer::class);
    }
}
