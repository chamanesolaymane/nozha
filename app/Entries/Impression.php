<?php

namespace App\Entries;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Impression extends Model
{
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function offer(){
        return $this->belongsTo(Offer::class);
    }
}
