<?php

namespace App\Entries;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
      'active' => 'boolean'
    ];


    public function item(){
        return $this->belongsTo(Item::class);
    }

    public function reservations(){
        return $this->hasMany(Reservation::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function impressions(){
        return $this->hasMany(Impression::class);
    }
}
