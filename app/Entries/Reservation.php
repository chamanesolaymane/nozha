<?php

namespace App\Entries;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $guarded = ['id'];

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    public function client(){
        return $this->belongsTo(User::class);
    }
}
