<?php


namespace App\Contracts;


use App\User;
use Illuminate\Database\Eloquent\Collection;

interface IUsers
{
    public function list() : Collection;

    public function create(array $data) : User;

    public function update(int $id , array $data) : void;

    public function find(int $id) : User;

    public function  delete(int $id) : void;

    public function activate(int $id) : void;

}
