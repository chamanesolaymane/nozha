<?php


namespace App\Contracts;


use App\Entries\Reservation;
use Illuminate\Database\Eloquent\Collection;

interface IReservations
{

    public function list() : Collection;

    public function create(array $data) : Reservation;

    public function update(array $data): void;

    public function find(int $id) : Reservation;

    public function delete(int $id) : void;

    public function listByCurrentUser() : Collection;

    public function toggleRead(int $id) : void;
}
