<?php


namespace App\Contracts;


use App\Entries\Item;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

interface IItems
{
    public function list() : Collection;

    public function create(array $data , UploadedFile $avatar = null , UploadedFile $cover = null) : Item;

    public function update(int $id ,array $data , UploadedFile $avatar = null , UploadedFile $cover = null) : Item;

    public function find(int $id) : Item;

    public function delete(int $id);

    public function activate (int $id);

}
