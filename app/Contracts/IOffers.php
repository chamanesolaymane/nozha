<?php


namespace App\Contracts;



use App\Entries\Offer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

interface IOffers
{
    public function list() : Collection;

    public function create(array  $data , UploadedFile $cover = null) : void;

    public function update(int $id , array  $data , UploadedFile $cover = null);

    public function find(int $id) : Offer;

    public function delete(int $id) : void;

    public function activate(int $id) : void;

    public function sponsored(int $id) : void ;

    public function listByType(string $type);

    public function search(Request $request);

    public function comment(array $date) : void;

    public function impression(array $data) : void;

    public function offersByName(string $name);
}
