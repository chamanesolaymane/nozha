<?php

namespace App\Providers;

use App\Contracts\IItems;
use App\Contracts\IOffers;
use App\Contracts\IReservations;
use App\Contracts\IUsers;
use App\Repositories\ItemsRepository;
use App\Repositories\OffersRepository;
use App\Repositories\ReservationsRepository;
use App\Repositories\UsersRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesService extends ServiceProvider
{
    public $bindings = [
      IItems:: class => ItemsRepository::class,
        IOffers::class => OffersRepository::class,
        IUsers::class => UsersRepository::class,
        IReservations::class => ReservationsRepository::class
    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
