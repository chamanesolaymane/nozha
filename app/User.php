<?php

namespace App;

use App\Entries\Comment;
use App\Entries\Impression;
use App\Entries\Reservation;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'active' => 'boolean'
    ];

    public function getFullNameAttribute(){
        return $this->last_name . ' '.$this->first_name;
    }

    public function reservations(){
        return $this->hasMany(Reservation::class);
    }

    public function impressions(){
        return $this->hasMany(Impression::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

}
